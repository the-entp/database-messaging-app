package com.example.messaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import android.os.Bundle;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {
	private SharedPreferences mPrefs;
	private LinkedList<HashMap<String, Integer>> values = new LinkedList<HashMap<String, Integer>>();
	private HashMap<String, Integer> perm_values = new HashMap<String, Integer>();
	private LinkedList<HashMap<Integer, Integer>> num_values = new LinkedList<HashMap<Integer, Integer>>();
	private boolean needCommit = false;
	private Pattern compileSet;
	private Pattern compileGet;
	private Pattern compileUnset;
	private Pattern compileNumEq;
	private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	String patternSet = "SET\\s\\p{Alpha}+\\s\\p{Digit}+";
		String patternGet = "GET\\s\\p{Alpha}+";
		String patternUnset = "UNSET\\s\\p{Alpha}+";
		String patternNumEq = "NUMEQUALTO\\s\\p{Digit}+";
		compileSet = Pattern.compile(patternSet);
		compileGet = Pattern.compile(patternGet);
		compileUnset = Pattern.compile(patternUnset);
		compileNumEq = Pattern.compile(patternNumEq);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.listview);
        HashMap<Integer, Integer> numEqSeed = new HashMap<Integer, Integer>();
        num_values.add(numEqSeed);
        Button submit = (Button) findViewById(R.id.submit_message);
        submit.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void saveTempVals() {

    	// make the temporary values permanent
    	for (String val : values.getLast().keySet()) {
    		perm_values.put(val, values.getLast().get(val));
    		Integer varVal = values.getLast().get(val);
    		if (num_values.getLast().containsKey(varVal)) {
				num_values.getLast().put(varVal, num_values.getLast().get(varVal) + 1);
			} else {
				num_values.getLast().put(varVal, 1);
			}
    		
    	}
    	HashMap<Integer, Integer> finalNumEq = num_values.getLast();
    	num_values.clear();
    	num_values.add(finalNumEq);
    	values.clear();
    }
    
    public void processSet(String result) {
    	String[] param = result.split(" ");
		String varName = param[1];
		Integer varVal = Integer.parseInt(param[2]);
		if (!needCommit) {
			// if you're setting a different value for a variable
			if (perm_values.containsKey(varName)) {
				Integer replacedVal = perm_values.get(varName);
				if (num_values.getLast().containsKey(replacedVal)) {
					num_values.getLast().put(replacedVal, num_values.getLast().get(replacedVal) - 1);
				}		
			} else {
				if (num_values.getLast().containsKey(varVal)) {
					num_values.getLast().put(varVal, num_values.getLast().get(varVal) + 1);
				} else {
					num_values.getLast().put(varVal, 1);
					System.out.println("I should be here " + varVal);
				}
			}
			perm_values.put(varName, varVal);
			
			
		} else {
			values.getLast().put(varName, varVal);
			if (num_values.getLast().containsKey(varVal)) {
				num_values.getLast().put(varVal, num_values.getLast().get(varVal) + 1);
			} else {
				num_values.getLast().put(varVal, 1);
			}
		}
    	
    }
    
    public void processGet(String result) {
    	String[] param = result.split(" ");
		String varName = param[1];
		Integer val = null;
		if (values.size() != 0) {
			val = values.getLast().get(varName);
		}
		if (perm_values.size() != 0) {
			val = perm_values.get(varName);
		} 
		if (val != null) {
			text.append(Integer.toString(val));
		} else {
			text.append("NULL");
		}
    }
    
    public void processBegin() {
    	HashMap<String, Integer> valStack = new HashMap<String,Integer>();
    	
		if (values.size() == 0) {
			for (String key: perm_values.keySet()) {
				valStack.put(key, perm_values.get(key));
			}
			
		} else {
			// add previous values
			HashMap<String, Integer> prev = values.getLast();
			for (String key: prev.keySet()) {
				valStack.put(key, prev.get(key));
			}
			HashMap<Integer, Integer> numEqStack = new HashMap<Integer, Integer>();
			HashMap<Integer, Integer> numEqPrev = num_values.getLast();
			for (Integer key: numEqPrev.keySet()) {
				numEqStack.put(key, numEqPrev.get(key));
			}
			num_values.add(numEqStack);
		}
		
		values.addLast(valStack);
		needCommit = true;
    }



	@Override
	public void onClick(View v) {
		EditText msg = (EditText) findViewById(R.id.blank_space);
		if (msg.getText().toString().equals("BEGIN")) {
			processBegin();
		} else if (msg.getText().toString().equals("END")) {
			finish();
		} else if (msg.getText().toString().equals("ROLLBACK")) {
			if (values.size() == 0) {
				text.append("NO TRANSACTION");
			} else {
				values.removeLast();
				
			}	
			if (num_values.size() != 0) {
				num_values.removeLast();
			}
			
		} else if (msg.getText().toString().equals("COMMIT")) {
			saveTempVals();
			needCommit = false;
		} else {
			Matcher matcherSet = compileSet.matcher(msg.getText().toString());
			Matcher matcherGet = compileGet.matcher(msg.getText().toString());
			Matcher matcherUnset = compileUnset.matcher(msg.getText().toString());
			Matcher matcherNumEq = compileNumEq.matcher(msg.getText().toString());
			if (matcherSet.find()) {
				String result = matcherSet.group(0);
				processSet(result);
				
			} else if (matcherGet.find()) {
				String result = matcherGet.group(0);
				processGet(result);
				
			} else if (matcherUnset.find()) {
				String result = matcherUnset.group(0);
				processUnset(result);
				
			} else if (matcherNumEq.find()) {
				String result = matcherNumEq.group(0);
				processNumEq(result);
				
			}	
		}
		text.append("\n");
		msg.getText().clear();
		
	}
	
	public void processNumEq(String result) {
		String[] param = result.split(" ");
		Integer numEq = num_values.getLast().get(Integer.parseInt(param[1]));
		if (numEq == null) {
			text.append(Integer.toString(0));
		} else {
			text.append(Integer.toString(numEq));
		}
		
	}
	
	
	public void processUnset(String result) {
		String[] param = result.split(" ");
		if (values.size() != 0){
			if (values.getLast().containsKey(param[1])) {
				Integer varVal = values.getLast().get(param[1]);
				values.getLast().remove(param[1]);
				
	    		if (num_values.getLast().containsKey(varVal)) {
					num_values.getLast().put(varVal, num_values.getLast().get(varVal) - 1);
				} 
			}
		}
		else if (perm_values.size() != 0) {
			if (perm_values.containsKey(param[1])) {
				Integer varVal = perm_values.get(param[1]);
				perm_values.remove(param[1]);
	    		if (num_values.getLast().containsKey(varVal)) {
					num_values.getLast().put(varVal, num_values.getLast().get(varVal) - 1);
				} 
			}
		} 
		
	}
    
}
